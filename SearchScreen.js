'use strict';

var React = require('react-native');
var {
  Linking,
  ActivityIndicatorIOS,
  ListView,
  Platform,
  ProgressBarAndroid,
  StyleSheet,
  Text,
  View,
} = React;

const xml_parser = require('xml-parser');

var TimerMixin = require('react-timer-mixin');

var invariant = require('fbjs/lib/invariant');
var dismissKeyboard = require('dismissKeyboard');

var URLCell = require('./URLCell');
var SearchBar = require('./SearchBar');

var himalaya = require('himalaya');


var resultsCache = {
  dataForQuery: {},
};


var LOADING = {};

var SearchScreen = React.createClass({
  mixins: [TimerMixin],
  timeoutID: (null: any),

  getInitialState: function() {
    return {
      isLoading: false,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      filter: '',
      queryNumber: 0,
    };
  },


  componentDidMount: function() {
    //this.searchURL('');
  },

  _urlForQuery: function(query: string): string {
    //var apiKey = API_KEYS[this.state.queryNumber % API_KEYS.length];
    if (query) {
      if (query.indexOf('http://') === -1 && query.indexOf('https://') === -1) {
        query = 'http://' + query;
      }
      return (
        query
      );
    } else {
      // With no query, load default/no URL
      return (
        ""
      );
    }
  },

  recursiveLinkFinder: function(JSONobject: Array<any>, URLString: string ): Array<any> {  
    //var myJSONSize = JSONobject.size();
    var myJSONSize = Object.keys(JSONobject).length;
    var myArray = [];

    for (var i = 0; i < myJSONSize; i++) {
      //check if current object contains an anchor link.
      if (JSONobject[i].type && (JSONobject[i].type.localeCompare("Element") == 0))
      {
        if (JSONobject[i].tagName && (JSONobject[i].tagName.localeCompare("a") == 0))
        {
          var urlLink = ""
          if (JSONobject[i].attributes.href && JSONobject[i].attributes.href[0] == '/')
          {
            urlLink = URLString.concat(JSONobject[i].attributes.href);
          }
          else
          {
            urlLink = JSONobject[i].attributes.href;
          }
          if ((JSONobject[i].children[0]) && (JSONobject[i].children[0].content != null) && (/\S/.test(JSONobject[i].children[0].content)))
          {
            var linkObject = {link:urlLink, tag:JSONobject[i].children[0].content};
          }
          else
          {
            var linkObject = {link:urlLink, tag:urlLink};
          }

          myArray.push(linkObject);

        }
      }

      //check if json child exists.
      if (JSONobject[i].children)
      {
          var returnedList = this.recursiveLinkFinder(JSONobject[i].children, URLString);
          if (Object.keys(returnedList).length > 0) {
            myArray = myArray.concat(returnedList);
          }
      }
    }

    return myArray;
  },


  searchURL: function(query: string) {
    this.timeoutID = null;

    this.setState({filter: query});

    var cachedResultsForQuery = resultsCache.dataForQuery[query];
    if (cachedResultsForQuery) {
      if (!LOADING[query]) {
        this.setState({
          dataSource: this.getDataSource(cachedResultsForQuery),
          isLoading: false
        });
      } else {
        this.setState({isLoading: true});
      }
      return;
    }

    LOADING[query] = true;
    resultsCache.dataForQuery[query] = null;
    this.setState({
      isLoading: true,
      queryNumber: this.state.queryNumber + 1,
    });

    fetch(this._urlForQuery(query))
      .then((response) => response.text())
      .catch((error) => {
        LOADING[query] = false;
        resultsCache.dataForQuery[query] = undefined;

        this.setState({
          dataSource: this.getDataSource([]),
          isLoading: false,
        });
      })
      .then((responseData) => {
        LOADING[query] = false;

        //console.log(xml_parser(responseData));
        //console.log(responseData);
        //var responseDataParsed = xml_parser(responseData);
        //console.log(responseDataParsed);

        var responseDataParsed = himalaya.parse(responseData);
        //console.log(responseDataParsed);

        //Run my recursive func
        var formattedData = this.recursiveLinkFinder(responseDataParsed,query);
        console.log(formattedData);


        resultsCache.dataForQuery[query] = formattedData;


        // if (this.state.filter !== query) {
        //   // do not update state if the query is stale
        //   return;
        // }

        this.setState({
          isLoading: false,
          dataSource: this.getDataSource(formattedData),
        });
      })
      .done();
  },

  getDataSource: function(anchors: Array<any>): ListView.DataSource {
    return this.state.dataSource.cloneWithRows(anchors);
    //return nothing for now
  },


  selectURL: function(url: Object) {
    if (Platform.OS === 'ios') {
    //   this.props.navigator.push({
    //     link: url.link,
    //     tag: url.tag,
    //     component: URLScreen,
    //     passProps: {url},
    //   });
    } else {
       dismissKeyboard();
    //   this.props.navigator.push({
    //     title: url.link,
    //     link: url.tag,
    //     name: 'url',
    //     url: url,
    //   });
    }

    Linking.canOpenURL(url.link).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url.link);
      } else {
        return Linking.openURL(url.link);
      }
    }).catch(err => console.error('An error occurred', err));
  },


  onSearchChange: function(event: Object) {
    var filter = event.nativeEvent.text.toLowerCase();

    this.clearTimeout(this.timeoutID);
    this.timeoutID = this.setTimeout(() => this.searchURL(filter), 1);
  },

  renderSeparator: function(
    sectionID: number | string,
    rowID: number | string,
    adjacentRowHighlighted: boolean
  ) {
    var style = styles.rowSeparator;
    if (adjacentRowHighlighted) {
        style = [style, styles.rowSeparatorHide];
    }
    return (
      <View key={'SEP_' + sectionID + '_' + rowID}  style={style}/>
    );
  },

  renderRow: function(
    URL: Object,
    sectionID: number | string,
    rowID: number | string,
    highlightRowFunc: (sectionID: ?number | string, rowID: ?number | string) => void,
  ) {
    return (
      <URLCell
        key={URL.id}
        onSelect={() => this.selectURL(URL)}
        onHighlight={() => highlightRowFunc(sectionID, rowID)}
        onUnhighlight={() => highlightRowFunc(null, null)}
        URL={URL}
      />
    );
  },

  render: function() {
    var content = this.state.dataSource.getRowCount() === 0 ?
      <NoURLs
        filter={this.state.filter}
        isLoading={this.state.isLoading}
      /> :
      <ListView
        ref="listview"
        renderSeparator={this.renderSeparator}
        dataSource={this.state.dataSource}
        renderFooter={this.renderFooter}
        renderRow={this.renderRow}
        onEndReached={this.onEndReached}
        automaticallyAdjustContentInsets={false}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={true}
        showsVerticalScrollIndicator={false}
      />;

    return (
      <View style={styles.container}>
        <SearchBar
          onSearchChange={this.onSearchChange}
          onSubmitEditing={this.props.onSearchChange}
          isLoading={this.state.isLoading}
        />
        <View style={styles.separator} />
        {content}
      </View>
    );
  },
});

var NoURLs = React.createClass({
  render: function() {
    var text = '';
    if (this.props.isLoading) {
      text = `Loading Results...`;
    } else if (this.props.filter) {
      text = `No results for "${this.props.filter}"`;
    } else if (!this.props.isLoading) {
      text = 'Please enter a URL...';
    }

    return (
      <View style={[styles.container, styles.centerText]}>
        <Text style={styles.noURLText}>{text}</Text>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  centerText: {
    alignItems: 'center',
  },
  noURLText: {
    marginTop: 80,
    color: '#888888',
  },
  separator: {
    height: 1,
    backgroundColor: '#eeeeee',
  },
  scrollSpinner: {
    marginVertical: 20,
  },
  rowSeparator: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    height: 1,
    marginLeft: 4,
  },
  rowSeparatorHide: {
    opacity: 0.0,
  },
});

module.exports = SearchScreen;







